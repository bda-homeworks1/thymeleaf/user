package com.company.thymeleaf.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class User {
    @Id
    @GeneratedValue()
   private Long id;

    @Column(name = "name")
   public String name;

    @Column(name = "surname")
   public String surname;

    @Column(name = "age")
   public Long age;

}
