package com.company.thymeleaf.service.impl;

import com.company.thymeleaf.entity.User;
import com.company.thymeleaf.repository.UserRepository;
import com.company.thymeleaf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public <S extends User> S save(S s) {
        return userRepository.save(s);
    }
}
