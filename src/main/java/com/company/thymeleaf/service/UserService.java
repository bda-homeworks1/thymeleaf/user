package com.company.thymeleaf.service;

import com.company.thymeleaf.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAll();

    Optional<User> findById(Long id);

    <S extends User> S save(S s);
}
