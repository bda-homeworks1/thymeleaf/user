package com.company.thymeleaf.controller;

import com.company.thymeleaf.entity.User;
import com.company.thymeleaf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
//@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping(value = ("/"))
    public String createUserForm(Model model){
        model.addAttribute("user",new User());
        return "index";
    }

    @PostMapping(value = "/register")
    public String register(@ModelAttribute User user, Model model){
        userService.save(user);
        System.out.println(user.toString());
        model.addAttribute("user", user);
        return  "index";
    }
}
